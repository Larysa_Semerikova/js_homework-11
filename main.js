const form = document.querySelector(".password-form");
const btn = document.querySelector(".btn");

form.addEventListener("click", (event) => {
      let target = event.target.dataset.type;
      if (target != undefined){
      let input = document.querySelector(target);
        if (input.type === "password"){
            input.type = "text";
            event.target.classList.replace("fa-eye", "fa-eye-slash");
        } else {
            input.type = "password";
            event.target.classList.replace("fa-eye-slash", "fa-eye");
          }
        }
   
    })


btn.addEventListener("click", (event) => {
  const password = document.getElementById("password")
  const confirmPassword = document.getElementById("confirm-password")

  event.preventDefault();

  if (password.value === "" || confirmPassword.value === "" || password.value !== confirmPassword.value) {
    let p = document.createElement("p")
    p.innerHTML = "Потрібно ввести однакові значення!"
    p.style.color = "red"
    confirmPassword.insertAdjacentElement("afterend", p)
    setTimeout(() => p.remove(), 1500);
  } else {
    alert("You are welcome");
  }

});

